<?php
require_once('config.php');

$code = $mysqli->real_escape_string($_GET['id']);

$sql = "SELECT * FROM `mails` WHERE `code`='$code'";
if ($qry = $mysqli->query($sql)) {
    if ($line = $qry->fetch_array()){
        $content = str_replace(':hide_here:', 'display: none !important;', $line['content']);
        echo $content.'
        <script type="text/javascript">
            window.onload = function() { window.print(); }
        </script>';
    }

} else {
  echo "Error: " . $sql . "<br>" . $mysqli->error;
  die();
}