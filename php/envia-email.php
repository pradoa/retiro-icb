<?
error_reporting(E_ERROR | E_PARSE);

//Destinatário do Email, caso seja mais de 1, separe os emails por virgula.
if(!isset($_GET['destino'])) {
$destino = $mail;
} else {
$destino = $_GET['destino'];	
}

// Codigo
if(isset($_GET['code'])) {
	$code = $_GET['code'];
} 

// Nome
if(isset($_GET['name'])) {
	$name = $_GET['name'];
} 

// Tipo 
if(isset($_GET['type'])) {
	$type = $_GET['type'];
} 

//Nome do remetente do email
$servidor_from = "ICB Maua";

//Servidor SMTP
$servidor_smtp = "mail.icb.tv";

//Usuário do email
$servidor_usuario = "noreply@icb.tv";

//Senha do email
$servidor_senha = "noreply123";

//Porta do Servidor. Geralmente 587.
$servidor_porta = 25;

//URL do Logotipo que vai no email
$url_logotipo = "http://conferencia.mosaico.tv/static/media/logo.a2bf6a56.png";
$url_logo_fb = "http://conferencia.mosaico.tv/static/media/facebook-dark-gray.png";
$url_logo_ig = "http://conferencia.mosaico.tv/static/media/instagram-dark-gray.png";


//NAO MEXER NAS LINHAS ABAIXO

function get_body($print, $url_logo_fb, $url_logo_ig, $url_logotipo, $name, $code, $type, $paylink){
$msg_body = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <!--[if !mso]><!-->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!--<![endif]-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="format-detection" content="telephone=no">
  <meta name="x-apple-disable-message-reformatting">
  <title></title>
  <style type="text/css">
    @media screen {
      @font-face {
        font-family: \'Fira Sans\';
        font-style: normal;
        font-weight: 400;
        src: local(\'Fira Sans Regular\'), local(\'FiraSans-Regular\'), url(https://fonts.gstatic.com/s/firasans/v8/va9E4kDNxMZdWfMOD5Vvl4jLazX3dA.woff2) format(\'woff2\');
        unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
      }
      @font-face {
        font-family: \'Fira Sans\';
        font-style: normal;
        font-weight: 400;
        src: local(\'Fira Sans Regular\'), local(\'FiraSans-Regular\'), url(https://fonts.gstatic.com/s/firasans/v8/va9E4kDNxMZdWfMOD5Vvk4jLazX3dGTP.woff2) format(\'woff2\');
        unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
      }
      @font-face {
        font-family: \'Fira Sans\';
        font-style: normal;
        font-weight: 500;
        src: local(\'Fira Sans Medium\'), local(\'FiraSans-Medium\'), url(https://fonts.gstatic.com/s/firasans/v8/va9B4kDNxMZdWfMOD5VnZKveRhf6Xl7Glw.woff2) format(\'woff2\');
        unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
      }
      @font-face {
        font-family: \'Fira Sans\';
        font-style: normal;
        font-weight: 500;
        src: local(\'Fira Sans Medium\'), local(\'FiraSans-Medium\'), url(https://fonts.gstatic.com/s/firasans/v8/va9B4kDNxMZdWfMOD5VnZKveQhf6Xl7Gl3LX.woff2) format(\'woff2\');
        unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
      }
      @font-face {
        font-family: \'Fira Sans\';
        font-style: normal;
        font-weight: 700;
        src: local(\'Fira Sans Bold\'), local(\'FiraSans-Bold\'), url(https://fonts.gstatic.com/s/firasans/v8/va9B4kDNxMZdWfMOD5VnLK3eRhf6Xl7Glw.woff2) format(\'woff2\');
        unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
      }
      @font-face {
        font-family: \'Fira Sans\';
        font-style: normal;
        font-weight: 700;
        src: local(\'Fira Sans Bold\'), local(\'FiraSans-Bold\'), url(https://fonts.gstatic.com/s/firasans/v8/va9B4kDNxMZdWfMOD5VnLK3eQhf6Xl7Gl3LX.woff2) format(\'woff2\');
        unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
      }
      @font-face {
        font-family: \'Fira Sans\';
        font-style: normal;
        font-weight: 800;
        src: local(\'Fira Sans ExtraBold\'), local(\'FiraSans-ExtraBold\'), url(https://fonts.gstatic.com/s/firasans/v8/va9B4kDNxMZdWfMOD5VnMK7eRhf6Xl7Glw.woff2) format(\'woff2\');
        unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
      }
      @font-face {
        font-family: \'Fira Sans\';
        font-style: normal;
        font-weight: 800;
        src: local(\'Fira Sans ExtraBold\'), local(\'FiraSans-ExtraBold\'), url(https://fonts.gstatic.com/s/firasans/v8/va9B4kDNxMZdWfMOD5VnMK7eQhf6Xl7Gl3LX.woff2) format(\'woff2\');
        unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
      }
    }

    #outlook a {
      padding: 0;
    }

    .ReadMsgBody,
    .ExternalClass {
      width: 100%;
    }

    .ExternalClass,
    .ExternalClass p,
    .ExternalClass td,
    .ExternalClass div,
    .ExternalClass span,
    .ExternalClass font {
      line-height: 100%;
    }

    div[style*="margin: 14px 0"],
    div[style*="margin: 16px 0"] {
      margin: 0 !important;
    }

    table,
    td {
      mso-table-lspace: 0;
      mso-table-rspace: 0;
    }

    table,
    tr,
    td {
      border-collapse: collapse;
    }

    body,
    td,
    th,
    p,
    div,
    li,
    a,
    span {
      -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
      mso-line-height-rule: exactly;
    }

    img {
      border: 0;
      outline: none;
      line-height: 100%;
      text-decoration: none;
      -ms-interpolation-mode: bicubic;
    }

    a[x-apple-data-detectors] {
      color: inherit !important;
      text-decoration: none !important;
    }

    body {
      margin: 0;
      padding: 0;
      width: 100% !important;
      -webkit-font-smoothing: antialiased;
    }

    .pc-gmail-fix {
      display: none;
      display: none !important;
    }

    @media screen and (min-width: 621px) {
      .pc-email-container {
        width: 620px !important;
      }
    }

    @media screen and (max-width:620px) {
      .pc-sm-p-24-20-30 {
        padding: 24px 20px 30px !important
      }
      .pc-sm-mw-100pc {
        max-width: 100% !important
      }
      .pc-sm-ta-center {
        text-align: center !important
      }
      .pc-sm-p-35-30 {
        padding: 35px 30px !important
      }
      .pc-sm-p-35-10-15 {
        padding: 35px 10px 15px !important
      }
      .pc-sm-p-21-10-14 {
        padding: 21px 10px 14px !important
      }
      .pc-sm-h-20 {
        height: 20px !important
      }
    }

    @media screen and (max-width:525px) {
      .pc-xs-p-15-10-20 {
        padding: 15px 10px 20px !important
      }
      .pc-xs-h-100 {
        height: 100px !important
      }
      .pc-xs-br-disabled br {
        display: none !important
      }
      .pc-xs-fs-30 {
        font-size: 30px !important
      }
      .pc-xs-lh-42 {
        line-height: 42px !important
      }
      .pc-xs-p-25-20 {
        padding: 25px 20px !important
      }
      .pc-xs-p-25-0-5 {
        padding: 25px 0 5px !important
      }
      .pc-xs-p-5-0 {
        padding: 5px 0 !important
      }
    }
  </style>
  <!--[if mso]>
    <style type="text/css">
        .pc-fb-font {
            font-family: Helvetica, Arial, sans-serif !important;
        }
    </style>
    <![endif]-->
  <!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]-->
  <meta id="bbafmabaelnnkondpfpjmdklbmfnbmol">
</head>
<body style="background-color: #f4f4f4; width: 100% !important; margin: 0; padding: 0; mso-line-height-rule: exactly; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;" class="">
  <span style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">Detalhes sobre a inscrição – </span>
  <table class="pc-email-body" width="100%" bgcolor="#f4f4f4" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background-color: #f4f4f4; table-layout: fixed;">
    <tbody>
      <tr>
        <td class="pc-email-body-inner" align="center" valign="top">
          <!--[if gte mso 9]>
            <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
                <v:fill type="tile" src="" color="#f4f4f4"/>
            </v:background>
            <![endif]-->
          <!--[if (gte mso 9)|(IE)]><table width="620" align="center" border="0" cellspacing="0" cellpadding="0" role="presentation"><tr><td width="620" align="center" valign="top"><![endif]-->
          <table class="pc-email-container" width="100%" align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="margin: 0 auto; max-width: 620px;">
            <tbody>
              <tr>
                <td align="left" valign="top" style="padding: 0 10px;">
                  <table width="100%" border="0" cellpadding="0" cellspacing="0" role="presentation">
                    <tbody>
                      <tr>
                        <td height="20" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
                      </tr>
                    </tbody>
                  </table>
                  <!-- BEGIN MODULE: Header 1 -->
                  <table width="100%" border="0" cellpadding="0" cellspacing="0" role="presentation">
                    <tbody>
                      <tr>
                        <td bgcolor="#000000" align="center" valign="top" style="background-color: #000000; background-position: top center; background-size: cover; border-radius: 8px; box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1)">
                          <!--[if gte mso 9]>
            <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width: 600px;">
                <v:fill type="frame" src="" color="#000000"></v:fill>
                <v:textbox style="mso-fit-shape-to-text: true;" inset="0,0,0,0">
                    <div style="font-size: 0; line-height: 0;">
                        <table width="600" border="0" cellpadding="0" cellspacing="0" role="presentation" align="center">
                            <tr>
                                <td style="font-size: 14px; line-height: 1.5;" valign="top">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" role="presentation">
                                        <tr>
                                            <td colspan="3" height="24" style="line-height: 1px; font-size: 1px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="30" style="line-height: 1px; font-size: 1px;" valign="top">&nbsp;</td>
                                            <td valign="top" align="left">
            <![endif]-->
                          <!--[if !gte mso 9]><!-->
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" role="presentation">
                            <tbody>
                              <tr>
                                <td class="pc-sm-p-24-20-30 pc-xs-p-15-10-20" style="padding: 24px 30px 40px;" valign="top">
                                  <!--<![endif]-->
                                  <table width="100%" border="0" cellpadding="0" cellspacing="0" role="presentation">
                                    <tbody>
                                      <tr>
                                        <td style="font-size: 0;" valign="top">
                                          <!--[if (gte mso 9)|(IE)]><table width="100%" border="0" cellspacing="0" cellpadding="0" role="presentation"><tr><td width="255" valign="middle"><![endif]-->
                                          <div class="pc-sm-mw-100pc" style="display: inline-block; width: 100%; max-width: 255px; vertical-align: middle;">
                                            <table width="100%" border="0" cellpadding="0" cellspacing="0" role="presentation">
                                              <tbody>
                                                <tr>
                                                  <td class="pc-sm-ta-center" valign="top" style="padding: 10px;">
                                                    <a href="http://icbmaua.com.br" style="text-decoration: none;"><img src="'.$url_logotipo.'" width="180" height="51" alt="" style="max-width: 100%; height: auto; border: 0; line-height: 100%; outline: 0; -ms-interpolation-mode: bicubic; font-size: 14px; color: #ffffff;"></a>
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </div>
                                          <!--[if (gte mso 9)|(IE)]></td><td width="285" valign="middle"><![endif]-->
                                          <div class="pc-sm-mw-100pc" style="display: inline-block; width: 100%; max-width: 285px; vertical-align: middle;">
                                            <table width="100%" border="0" cellpadding="0" cellspacing="0" role="presentation">
                                              <tbody>
                                                <tr>
                                                  <td class="pc-sm-ta-center" valign="top" style="padding: 9px 10px 10px; line-height: 18px; font-family: Helvetica, sans-serif; font-size: 14px;">
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </div>
                                          <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                        </td>
                                      </tr>
                                    </tbody>
                                    <tbody>
                                      <tr>
                                        <td class="pc-xs-h-100" height="0" style="line-height: 1px; font-size: 1px">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td class="pc-fb-font" style="padding: 0 10px;" valign="top">
                                          <a style="text-decoration: none; font-family: \'Fira Sans\', Helvetica, Arial, sans-serif; font-size: 12px; font-weight: 500; color: #ffffff">icb.tv/conferencia</a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td class="pc-xs-fs-30 pc-xs-lh-42 pc-fb-font" style="padding: 13px 10px 0; letter-spacing: -0.7px; line-height: 46px; font-family: \'Fira Sans\', Helvetica, Arial, sans-serif; font-size: 32px; font-weight: 800; color: #ffffff" valign="top">Detalhes da Inscrição</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <!--[if !gte mso 9]><!-->
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <!--<![endif]-->
                          <!--[if gte mso 9]>
                                            </td>
                                            <td width="30" style="line-height: 1px; font-size: 1px;" valign="top">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" height="40" style="line-height: 1px; font-size: 1px;">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </v:textbox>
            </v:rect>
            <![endif]-->
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <!-- END MODULE: Header 1 -->
                  <!-- BEGIN MODULE: Call to Action 0 -->
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" role="presentation">
                    <tbody>
                      <tr>
                        <td height="8" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" role="presentation">
                    <tbody>
                      <tr>
                        <td class="pc-sm-p-35-30 pc-xs-p-25-20" style="padding: 40px 30px 32px; background-color: #ffffff; border-radius: 8px; box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);" valign="top" bgcolor="#ffffff">
                          <table border="0" cellpadding="0" cellspacing="0" width="100%" role="presentation">
                            <tbody>
                              <tr>
                                <td class="pc-xs-fs-30 pc-xs-lh-42 pc-fb-font" style="font-family: \'Fira Sans\', Helvetica, Arial, sans-serif; font-size: 32px; font-weight: 900; line-height: 36px; letter-spacing: -0.6px; color: #151515; text-align: center" valign="top">'.$name.'</td>
                              </tr>
                            </tbody>
                            <tbody>
                              <tr>
                                <td class="pc-fb-font" style="font-family: \'Fira Sans\', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 300; line-height: 28px; color: #9B9B9B; text-align: center;" valign="top">'.$type.'</td>
                              </tr>
                              <tr>
                                <td height="11" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <!-- END MODULE: Call to Action 0 -->
                  <!-- BEGIN MODULE: Call to Action 1 -->
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" role="presentation">
                    <tbody>
                      <tr>
                        <td height="8" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" role="presentation">
                    <tbody>
                      <tr>
                        <td class="pc-sm-p-35-30 pc-xs-p-25-20" style="padding: 40px 30px 32px; background-color: #ffffff; border-radius: 8px; box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);" valign="top" bgcolor="#ffffff">
                          <table border="0" cellpadding="0" cellspacing="0" width="100%" role="presentation">
						  <tbody>
							<tr>
							  <td class="pc-fb-font" style="font-family: \'Fira Sans\', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 300; line-height: 28px; color: #9B9B9B; text-align: center;" valign="top">Este é seu código de inscrição:</td>
							</tr>
							<tr>
							  <td height="28" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
							</tr>
						  </tbody>
                            <tbody>
                              <tr>
                                <td class="pc-xs-fs-30 pc-xs-lh-42 pc-fb-font" style="font-family: \'Fira Sans\', Helvetica, Arial, sans-serif; font-size: 62px; font-weight: 900; line-height: 46px; letter-spacing: -0.6px; color: #151515; text-align: center" valign="top">'.$code.'</td>
                              </tr>
                              <tr>
                                <td height="20" style="font-size: 1px; line-height: 1px">&nbsp;</td>
                              </tr>
                            </tbody>
                            <tbody style=":hide_here:">
                              <tr>
                                <td style="padding: 8px 0;" valign="top" align="center">
                                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse: separate; border-spacing: 10px;">
                                    <tbody>
                                      <tr>
										<td valign="top" align="center">
											<a class="pc-fb-font" href="'.$paylink.'" target="blank" style="line-height: 1.5; text-decoration: none; word-break: break-word; font-weight: 500; display: block; font-family: \'Fira Sans\', Helvetica, Arial, sans-serif; font-size: 16px; color: #ffffff; padding: 13px 17px; border-radius: 8px; background-color: #427BDC;">Pagar Online</a>
										</td>
                                        <td valign="top" align="center">
                                          <a class="pc-fb-font" href="'.$print.'" target="blank" style="line-height: 1.5; text-decoration: none; word-break: break-word; font-weight: 500; display: block; font-family: \'Fira Sans\', Helvetica, Arial, sans-serif; font-size: 16px; color: #ffffff; padding: 13px 17px; border-radius: 8px; background-color: #427BDC;">Imprimir</a>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <!-- END MODULE: Call to Action 1 -->
                  <!-- BEGIN MODULE: Content 1 -->
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" role="presentation">
                    <tbody>
                      <tr>
                        <td height="8" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" role="presentation">
                    <tbody>
                      <tr>
                        <td class="pc-sm-p-35-10-15 pc-xs-p-25-0-5" style="padding: 40px 20px 20px; background-color: #ffffff; border-radius: 8px; box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);" valign="top" bgcolor="#ffffff">
                          <table border="0" cellpadding="0" cellspacing="0" width="100%" role="presentation">
                            <tbody>
                              <tr>
                                <td class="pc-fb-font" style="padding: 0 20px; text-align: center; font-family: \'Fira Sans\', Helvetica, Arial, sans-serif; font-size: 11px; font-weight: 700; line-height: 1.42; letter-spacing: -0.4px; color: #151515" valign="top">* Você também pode imprimir esta página e apresentar no dia!</td>
                              </tr>
                              <tr>
                                <td height="15" style="font-size: 1px; line-height: 1px">&nbsp;</td>
                              </tr>
                            </tbody>
                            <tbody>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <!-- END MODULE: Content 1 -->
                  <!-- BEGIN MODULE: Footer 1 -->
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" role="presentation">
                    <tbody>
                      <tr>
                        <td height="8" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" role="presentation">
                    <tbody>
                      <tr>
                        <td class="pc-sm-p-21-10-14 pc-xs-p-5-0" style="padding: 21px 20px 14px 20px; background-color: #000000; border-radius: 8px; box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1)" valign="top" bgcolor="#000000" role="presentation">
                          <table border="0" cellpadding="0" cellspacing="0" width="100%" role="presentation">
                            <tbody>
                              <tr>
                                <td style="font-size: 0;" valign="top">
                                  <!--[if (gte mso 9)|(IE)]><table width="100%" border="0" cellspacing="0" cellpadding="0" role="presentation"><tr><td width="280" valign="top"><![endif]-->
                                  <div class="pc-sm-mw-100pc" style="display: inline-block; width: 100%; max-width: 280px; vertical-align: top;">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" role="presentation">
                                      <tbody>
                                        <tr>
                                          <td style="padding: 20px;" valign="top">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" role="presentation">
                                              <tbody>
                                                <tr>
                                                  <td class="pc-fb-font" style="font-family: \'Fira Sans\', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 500; line-height: 24px; letter-spacing: -0.2px; color: #ffffff;" valign="top">Siga-nos nas Redes Sociais</td>
                                                </tr>
                                                <tr>
                                                  <td height="11" style="line-height: 1px; font-size: 1px;">&nbsp;</td>
                                                </tr>
                                              </tbody>
                                              <tbody>
                                                <tr>
                                                  <td class="pc-fb-font" style="font-family: \'Fira Sans\', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 20px; letter-spacing: -0.2px; color: #D8D8D8" valign="top">Estamos sempre por lá postando novidades.</td>
                                                </tr>
                                                <tr>
                                                  <td class="pc-sm-h-20" height="56" style="line-height: 1px; font-size: 1px;">&nbsp;</td>
                                                </tr>
                                              </tbody>
                                              <tbody>
                                                <tr>
                                                  <td style="font-family: Arial, sans-serif; font-size: 19px;" valign="top">
                                                    <a href="https://fb.com/icbmaua" style="text-decoration: none;"><img src="'.$url_logo_fb.'" width="20" height="20" alt="" style="border: 0; line-height: 100%; outline: 0; -ms-interpolation-mode: bicubic; color: #ffffff;"></a>
                                                    <span>&nbsp;&nbsp;</span>
                                                    <a href="https://instagram.com/icbmaua" style="text-decoration: none;"><img src="'.$url_logo_ig.'" width="21" height="20" alt="" style="border: 0; line-height: 100%; outline: 0; -ms-interpolation-mode: bicubic; color: #ffffff;"></a>
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                  <!--[if (gte mso 9)|(IE)]></td><td width="280" valign="top"><![endif]-->
                                  <div class="pc-sm-mw-100pc" style="display: inline-block; width: 100%; max-width: 280px; vertical-align: top;">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" role="presentation">
                                      <tbody>
                                        <tr>
                                          <td style="padding: 20px;" valign="top">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" role="presentation">
                                              <tbody>
                                                <tr>
                                                  <td class="pc-fb-font" style="font-family: \'Fira Sans\', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 500; line-height: 24px; letter-spacing: -0.2px; color: #ffffff;" valign="top">ICB Mauá</td>
                                                </tr>
                                                <tr>
                                                  <td height="11" style="line-height: 1px; font-size: 1px;">&nbsp;</td>
                                                </tr>
                                              </tbody>
                                              <tbody>
                                                <tr>
                                                  <td class="pc-fb-font" style="font-family: \'Fira Sans\', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 20px; letter-spacing: -0.2px; color: #D8D8D8" valign="top">Rua Luis Mariani, 56 - Centro - Mauá / SP</td>
                                                </tr>
                                                <tr>
                                                  <td class="pc-sm-h-20" height="45" style="line-height: 1px; font-size: 1px;">&nbsp;</td>
                                                </tr>
                                              </tbody>
                                              <tbody>
                                                <tr>
                                                  <td class="pc-fb-font" style="font-family: \'Fira Sans\', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 500; line-height: 24px; letter-spacing: -0.2px;" valign="top">
                                                    <a href="tel:+551145446618" style="text-decoration: none; color: #ffffff;">(11) 4544-6618</a>
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td height="9" style="line-height: 1px; font-size: 1px;">&nbsp;</td>
                                                </tr>
                                              </tbody>
                                              <tbody>
                                                <tr>
                                                  <td class="pc-fb-font" style="font-family: \'Fira Sans\', Helvetica, Arial, sans-serif; font-size: 14px; font-weight: 500; line-height: 24px;" valign="top">
                                                    <a href="mailto:contato@icbmaua.com.br" style="text-decoration: none; color: #1595E7;">contato@icbmaua.com.br</a>
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                  <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <!-- END MODULE: Footer 1 -->
                  <table width="100%" border="0" cellpadding="0" cellspacing="0" role="presentation">
                    <tbody>
                      <tr>
                        <td height="20" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
          <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
        </td>
      </tr>
    </tbody>
  </table>
  <!-- Fix for Gmail on iOS -->
  <div class="pc-gmail-fix" style="white-space: nowrap; font: 15px courier; line-height: 0;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </div>
</body>
</html>
';

return $msg_body;
}

$paylink = "http://icbmaua.com.br/proxy/getpaylink.php?code=$code";

require_once('config.php');
$body = $mysqli->real_escape_string(get_body('', $url_logo_fb, $url_logo_ig, $url_logotipo, $name, $code, $type, $paylink));
$sql = "INSERT INTO `mails` (`code`,`content`) VALUES ('$code','$body')";

if ($mysqli->query($sql) === TRUE) {
  $print_url = "http://icbmaua.com.br/proxy/print.php?id=$code";
} else {
  echo "Error: " . $sql . "<br>" . $mysqli->error;
  die();
}

include "class.phpmailer.php";
	
$mail = new PHPMailer();

$mail->IsSMTP();
$mail->SMTPAuth = true;
$mail->Host = $servidor_smtp;
$mail->Port = $servidor_porta;

$mail->Username = $servidor_usuario;
$mail->Password = $servidor_senha;

$mail->From = $servidor_usuario;
$mail->FromName = $servidor_from;
$mail->Subject = $mail_title;

$mail->Body = get_body($print_url, $url_logo_fb, $url_logo_ig, $url_logotipo, $name, $code, $type, $paylink);
$mail->AltBody = "Para ver essa mensagem, abra em um leitor de email compativel com HTML.";

$mail->CharSet = "utf-8";

$destinos = explode(",",$destino);

foreach($destinos as $para) {
	
	$mail->AddAddress($para);
	
}


if(!$mail->Send()) {
	echo "Problema ao enviar o email: " . $mail->ErrorInfo;
} else {
}
//header("Location:$retorno");

?>