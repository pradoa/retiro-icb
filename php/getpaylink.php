<?php
require_once('config.php');

ini_set('display_errors', 1);
error_reporting(E_ALL);

$code = $_GET['code'];

$sql = "SELECT `postdata` FROM `transactions` WHERE `code`='$code'";
if ($qry = $mysqli->query($sql)) {
  if ($line = $qry->fetch_array()){
    $_POST = json_decode($line['postdata']);

    $email = $PS_EMAIL;
    $token = $PS_TOKEN;
    $url = "https://ws.pagseguro.uol.com.br/v2/checkout?name=ICB&email=$email&token=$token";

    $data = $_POST;

    //open connection
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => http_build_query($data),
        CURLOPT_HTTPHEADER => array(
          "Accept: */*",
          "Cache-Control: no-cache",
          "Connection: keep-alive",
          "Content-Type: application/x-www-form-urlencoded",
          "Host: ws.pagseguro.uol.com.br",
          "Postman-Token: a4fb9f4f-cc00-47ae-a9b7-bfee4295b15e,4efbaeb8-15ef-46db-9357-4fc5c43d1085",
          "User-Agent: PostmanRuntime/7.11.0",
          "accept-encoding: gzip, deflate",
          "cache-control: no-cache",
        ),
      ));

    //execute post
    $result = curl_exec($curl);

    //close connection
    curl_close($curl);

    try {
      $xml = simplexml_load_string($result);

      if (sizeof($xml->code) > 0){
        // code means success, so we can save info to db
        
        header('Location: https://pagseguro.uol.com.br/v2/checkout/payment.html?code='.$xml->code);
      }
    } catch (Exception $ex) {
      die (var_dump($ex));
    }
  }

} else {
  echo "Error: " . $sql . "<br>" . $mysqli->error;
  die();
}

?>