<?php

require_once('config.php');

header("Access-Control-Allow-Origin: *");
header("Access-Control-Request-Method: GET, OPTIONS");
header("Access-Control-Expose-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization");
header("Access-Control-Request-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization");

header("Content-Type: application/json");

$results = [];

$sql = "SELECT * FROM `transactions`";

if ($qry = $mysqli->query($sql)) {
  while ($line = $qry->fetch_array()){
    if ($line['member'] == 1)
      $line['member'] = "Sim";
    else
      $line['member'] = "Não";

    array_push($results, $line);
  }

  echo json_encode($results);
} else {
  echo "Error: " . $sql . "<br>" . $mysqli->error;
  die();
}

?>