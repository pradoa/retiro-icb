<?php
require_once('config.php');

function unique_code($limit)
{
  return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $limit);
}

function getUniqueRef(){
  return strtoupper(unique_code(8));
}

$_POST['reference'] = getUniqueRef();

$email = $_GET['email'];
$token = $_GET['token'];
$url = $_GET['url'] . "&email=$email&token=$token";
$token = $_GET['token'];

$data = $_POST;

//open connection
$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => $url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => http_build_query($data),
    CURLOPT_HTTPHEADER => array(
      "Accept: */*",
      "Cache-Control: no-cache",
      "Connection: keep-alive",
      "Content-Type: application/x-www-form-urlencoded",
      "Host: ws.pagseguro.uol.com.br",
      "Postman-Token: a4fb9f4f-cc00-47ae-a9b7-bfee4295b15e,4efbaeb8-15ef-46db-9357-4fc5c43d1085",
      "User-Agent: PostmanRuntime/7.11.0",
      "accept-encoding: gzip, deflate",
      "cache-control: no-cache",
    ),
  ));

//execute post
$result = curl_exec($curl);

//close connection
curl_close($curl);


$xml = simplexml_load_string($result);

if (sizeof($xml->code) > 0){
  // code means success, so we can save info to db
  $name = $data['senderName'];
  $mail = $data['senderEmail'];
  $phone = $data['senderAreaCode'] . $data['senderPhone'];
  $address = $data['address'];
  $code = $data['reference'];
  $comp = explode('::', $data['shippingAddressComplement']);
  $member = $comp[0];
  $kit = $comp[1];
  $option = $comp[2];

  $postdata = json_encode($_POST);

  $result = str_replace('</date>', '</date><ref>'.$code.'</ref>', $result);

  header("Access-Control-Allow-Origin: *");
  echo $result;

  $sql = "INSERT INTO `transactions` (`name`,`email`,`phone`,`address`,`code`,`status`,`member`,`kit`,`option`,`postdata`) VALUES ('$name','$mail','$phone','$address','$code','0','$member','$kit','$option','$postdata')";

  if ($mysqli->query($sql) === TRUE) {
    $transactionCode = $xml->code;
    $mail_title = '=?UTF-8?B?'.base64_encode('Conferência Mosaico - Inscrição Realizada').'?=';
    $type = $kit == '0' ? 'Inscrição Simples' : 'Inscrição com Camiseta';
    include_once('envia-email.php');
  } else {
    echo "Error: " . $sql . "<br>" . $mysqli->error;
    die();
  }
}

?>