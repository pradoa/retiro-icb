export const SITE_BASE = 'http://retiro.icb.tv/';
export const PROXY_BASE = 'http://icbmaua.com.br/proxy/';

export const IN_PROD = process.env.REACT_APP_ENV === 'production';
export const IN_DEV = process.env.REACT_APP_ENV === 'development';