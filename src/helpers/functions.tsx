import { DeviceRoute } from './enums';

export const routeByDeviceWidth = () => (window.innerWidth <= 768) ? DeviceRoute.Mobile : DeviceRoute.Desktop;
export const toMoney = (value: number, decimais: number = 2) => {
    let tmps: number = value;
    let tmp: string = tmps.toFixed(decimais) + '';
    let neg: boolean = false;

    if (tmps - (Math.round(value)) == 0) {
        tmp = tmp + '';
    }

    if (tmp.indexOf(".")) {
        tmp = tmp.replace(".", "");
    }

    if (tmp.indexOf("-") == 0) {
        neg = true;
        tmp = tmp.replace("-", "");
    }

    if (tmp.length == 1) tmp = "0" + tmp;

    tmp = tmp.replace(/([0-9]{2})$/g, ",$1");

    if (tmp.length > 6)
        tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");

    if (tmp.length > 9)
        tmp = tmp.replace(/([0-9]{3}).([0-9]{3}),([0-9]{2}$)/g, ".$1.$2,$3");

    if (tmp.length === 12)
        tmp = tmp.replace(/([0-9]{3}).([0-9]{3}).([0-9]{3}),([0-9]{2}$)/g, ".$1.$2.$3,$4");

    if (tmp.length > 12)
        tmp = tmp.replace(/([0-9]{3}).([0-9]{3}).([0-9]{3}).([0-9]{3}),([0-9]{2}$)/g, ".$1.$2.$3.$4,$5");

    if (tmp.indexOf(".") == 0) tmp = tmp.replace(".", "");
    if (tmp.indexOf(",") == 0) tmp = tmp.replace(",", "0,");

    return (neg ? '-' + tmp : tmp);
};
export const clearForFormat = (text: string) => {
    if (!text)
        return "";

    let clearText = text;

    clearText = clearText.replace(/\-/g, '');
    clearText = clearText.replace(/\,/g, '');
    clearText = clearText.replace(/\//g, '');
    clearText = clearText.replace(/ /g, '');
    clearText = clearText.replace(/\(/g, '');
    clearText = clearText.replace(/\)/g, '');
    clearText = clearText.replace(/\_/g, '');

    return clearText;
};

export const formatDateEn = (date: string) => {
    date = clearForFormat(date);
    let year = date.substr(4, 4);
    let mon = date.substr(2, 2);
    let day = date.substr(0, 2);

    return `${year}-${mon}-${day}`;
};
export const formatDatePt = (date: string, orig?: boolean) => {
    if (orig) {
        date = clearForFormat(date);
        let year = date.substr(4, 4);
        let mon = date.substr(2, 2);
        let day = date.substr(0, 2);

        return `${day}/${mon}/${year}`;
    }

    date = clearForFormat(date);
    let year = date.substr(0, 4);
    let mon = date.substr(4, 2);
    let day = date.substr(6, 2);

    return `${day}/${mon}/${year}`;
};

export const validEmail = (mail: string) => {
    return (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail));
};

export const validDate = (inputText: string) => {
    let dateformat = /^(0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])[\/\-]\d{4}$/;
    // Match the date format through regular expression
    if (inputText.match(dateformat)) {
        let opera1 = inputText.split('/');
        let opera2 = inputText.split('-');
        let lopera1 = opera1.length;
        let lopera2 = opera2.length;
        let pdate = {};
        // Extract the string into month, date and year
        if (lopera1 > 1) {
            pdate = inputText.split('/');
        }
        else if (lopera2 > 1) {
            pdate = inputText.split('-');
        }
        let mm = parseInt(pdate[1]);
        let dd = parseInt(pdate[0]);
        let yy = parseInt(pdate[2]);
        // Create list of days of a month [assume there is no leap year by default]
        let ListofDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
        if (mm < 1 || mm > 12 || dd < 1 || dd > 31 || yy < 1000 || yy > new Date().getFullYear())
            return false;

        if (mm == 1 || mm > 2) {
            if (dd > ListofDays[mm - 1]) {
                return false;
            }
        }
        if (mm == 2) {
            let lyear = false;
            if ((!(yy % 4) && yy % 100) || !(yy % 400)) {
                lyear = true;
            }
            if ((lyear == false) && (dd >= 29)) {
                return false;
            }
            if ((lyear == true) && (dd > 29)) {
                return false;
            }
        }

        return true;
    }
    else {
        return false;
    }
};