import React, { PureComponent } from 'react';

export default class Header extends PureComponent<any, any> {
    toggleMobileMenu() {
        document.getElementsByTagName("body")[0].classList.toggle("offcanvas-menu");
    }

    render(): React.ReactNode {
        return (
            <div className="headerWrapper">
                <div className="site-mobile-menu">
                    <div className="site-mobile-menu-header text-right">
                        <a href="#" className="text-black" onClick={() => this.toggleMobileMenu()}>
                            <span className="fa fas fa-times"></span>
                        </a>
                    </div>
                    <div className="site-mobile-menu-body">
                        <ul className="site-nav-wrap">
                            <li className="active"><a href="/">Home</a></li>
                            <li className="cta"><a href="/ticket">Inscrição</a></li>
                        </ul>
                    </div>
                </div>

                <header className="site-navbar py-3" role="banner">
                    <div className="container-fluid">
                        <div className="row align-items-center">
                            <div className="col-11 col-xl-2">
                                <h1 className="mb-0">
                                    <a href="/" className="text-white h2 mb-0">
                                        <img className="logo" src={require('assets/images/logo.png')} />
                                    </a>
                                </h1>
                            </div>
                            <div className="col-12 col-md-10 d-none d-xl-block">
                                <nav className="site-navigation position-relative text-right" role="navigation">
                                    <ul className="site-menu js-clone-nav mx-auto d-none d-lg-block">
                                        <li className="active"><a href="/">Home</a></li>
                                        <li className="cta"><a href="/ticket">Inscrição</a></li>
                                    </ul>
                                </nav>
                            </div>
                            <div className="d-inline-block d-xl-none ml-md-0 mr-auto py-3">
                                <a href="#" className="site-menu-toggle js-menu-toggle text-white" onClick={() => this.toggleMobileMenu()}>
                                    <span className="fa fas fa-bars"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </header>
            </div >
        );
    }
}