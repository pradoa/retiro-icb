import React, { Component } from 'react';
import Header from 'components/PageStructure/Header';
import { get } from 'helpers/agent';
import { PROXY_BASE } from 'helpers/constants';
import { Table } from 'antd';

interface IProps {
}

interface IStates {
   data: any;
}

export default class StatusList extends Component<IProps, IStates> {
   constructor(props: any) {
      super(props);

      this.state = {
         data: []
      };
   }

   componentDidMount() {
      this.loadData();
   }

   async loadData() {
      let response = await get(`${PROXY_BASE}/status.php`, {
         headers: {
            "Content-Type": "application/json"
         }
      });
      console.log(response);

      this.setState({ data: response.data });
   }

   getStatus(status: number) {
      switch (status) {
         case 0:
         case 1: return "Aguardando Pagamento"; break;
         case 2: return "Pagamento em Análise"; break;
         case 3: return "Pago"; break;

         case 4:
         case 6:
         case 8: return "Pagamento Devolvido"; break;

         case 7: return "Cancelado"; break;

         default: return `Processando (${status})`; break;
      }
   }

   public render(): React.ReactNode {
      const { data } = this.state;

      const columns = [{
         title: 'ID',
         dataIndex: 'id',
         key: 'id'
      }, {
         title: 'Nome',
         dataIndex: 'name',
         key: 'name',
         render: (text: string) => <a href="javascript:;">{text}</a>,
      }, {
         title: 'Email',
         dataIndex: 'email',
         key: 'email',
      }, {
         title: 'Telefone',
         dataIndex: 'phone',
         key: 'phone',
      }, {
         title: 'Membro',
         dataIndex: 'member',
         key: 'member',
      }, {
         title: 'Camiseta',
         dataIndex: 'kit',
         key: 'kit',
      }, {
         title: 'Tamanho',
         dataIndex: 'option',
         key: 'option',
      }, {
         title: 'Cod. Inscrição',
         dataIndex: 'code',
         key: 'code',
         render: (text: string) => <a href="javascript:;">{text}</a>,
      }, {
         title: 'Status',
         dataIndex: 'status',
         key: 'status',
         render: (text: any) => this.getStatus(Number.parseInt(text, 0)),
      }];

      return (
         <div>
            <Header />

            <div className="site-section site-hero inner">
               <div className="container">
                  <div className="row align-items-center">
                     <div className="col-md-10">
                        <span className="d-block mb-3 caption" data-aos="fade-up">Verificar</span>
                        <h1 className="d-block mb-4" data-aos="fade-up" data-aos-delay="100">Inscrições</h1>
                     </div>
                  </div>
               </div>
            </div>

            <div className="site-section">
               <div className="col-12">
                  <Table dataSource={data} columns={columns} />
               </div>
            </div>
         </div>
      );
   }
}
