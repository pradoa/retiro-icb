import React, { Component } from 'react';
import Header from 'components/PageStructure/Header';

interface IProps {
}

interface IStates {
}

export default class Tickets extends Component<IProps, IStates> {
   constructor(props: any) {
      super(props);
   }

   public render(): React.ReactNode {
      return (
         <>
            <Header />

            <div className="site-section site-hero inner">
               <div className="container">
                  <div className="row align-items-center">
                     <div className="col-md-10">
                        <span className="d-block mb-3 caption">Faça a sua</span>
                        <h1 className="d-block mb-4">Inscrição</h1>
                     </div>
                  </div>
               </div>
            </div>

            <div className="site-section">
               <div className="container">

                  <div className="row">
                     <div className="col-md-12 col-lg-1 mb-5 mb-lg-0"></div>

                     <div className="col-md-6 col-lg-5 mb-5 mb-lg-0" data-aos="fade-up" data-aos-delay="300">
                        <div className="pricing border-secondary text-center">
                           <h2>Inscrição Simples</h2>
                           <div className="amount"><sup>R$</sup><span className="number">30</span> </div>
                           <ul className="list-unstyled mb-5">
                              <li>Pagamento Online no Cartão ou Boleto</li>
                              <li>Entrada para todos os dias</li>
                              <li>Não inclui Alimentação</li>
                              <li>Acesso à todas as ministrações</li>
                              <li>Sem Camiseta</li>
                           </ul>
                           <div><a href="/buy/1001/" className="btn btn-secondary px-4 py-2">Inscreva-se</a></div>
                        </div>
                     </div>

                     <div className="col-md-6 col-lg-5 mb-5 mb-lg-0" data-aos="fade-up" data-aos-delay="400">
                        <div className="pricing border-primary text-center">
                           <h2>Inscrição + Camiseta</h2>
                           <div className="amount"><sup>R$</sup><span className="number">60</span> </div>
                           <ul className="list-unstyled mb-5">
                              <li>Pagamento Online no Cartão ou Boleto</li>
                              <li>Entrada para todos os dias</li>
                              <li>Não inclui Alimentação</li>
                              <li>Acesso à todas as ministrações</li>
                              <li>Com Camiseta</li>
                           </ul>
                           <div><a href="/buy/1002/" className="btn btn-primary px-4 py-2">Inscreva-se</a></div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>

         </>
      );
   }
}
