const products = {
    1001: {
        name: "Inscrição Simples",
        value: 30.00,
        fields: [
            {
                name: `name`,
                label: `Nome Completo`,
                tip: false,
                required: true,
                validateErr: ``,
            },
            {
                name: `email`,
                label: `Email`,
                tip: false,
                required: true,
                validateErr: ``,
            },
            {
                name: `address`,
                label: `Endereço Completo`,
                placeholder: `Ex: Rua Carlos Gomes, 235 - Bairro das Oliveiras - São Paulo / SP`,
                tip: false,
                required: true,
                validateErr: ``,
            },
            {
                name: `ddd`,
                label: `DDD`,
                tip: false,
                required: true,
                validateErr: ``,
                max: 2,
                cols: 6
            },
            {
                name: `phone`,
                label: `Telefone (Apenas Números)`,
                tip: false,
                required: true,
                validateErr: ``,
                max: 9,
                cols: 18
            },
            {
                name: `member`,
                label: `Membro desta igreja local?`,
                tip: false,
                required: true,
                validateErr: ``,
                cols: 24,
                type: 'checkbox',
            },
        ]
    },
    1002: {
        name: "Inscrição com Camiseta",
        value: 60.00,
        fields: [
            {
                name: `name`,
                label: `Nome Completo`,
                tip: false,
                required: true,
                validateErr: ``,
            },
            {
                name: `email`,
                label: `Email`,
                tip: false,
                required: true,
                validateErr: ``,
            },
            {
                name: `address`,
                label: `Endereço Completo`,
                placeholder: `Ex: Rua Carlos Gomes, 235 - Bairro das Oliveiras - São Paulo / SP`,
                tip: false,
                required: true,
                validateErr: ``,
            },
            {
                name: `ddd`,
                label: `DDD`,
                tip: false,
                required: true,
                validateErr: ``,
                max: 2,
                cols: 6
            },
            {
                name: `phone`,
                label: `Telefone (Apenas Números)`,
                tip: false,
                required: true,
                validateErr: ``,
                max: 9,
                cols: 18
            },
            {
                name: `member`,
                label: `Membro desta igreja local?`,
                tip: false,
                required: true,
                validateErr: ``,
                cols: 24,
                type: 'checkbox',
            },
            {
                name: `size`,
                label: `Tamanho da Camiseta`,
                tip: false,
                required: true,
                validateErr: ``,
                cols: 24,
                type: 'select',
                options: [
                    {
                        text: 'Baby Look - P',
                        value: 'Baby Look - P',
                    },
                    {
                        text: 'Baby Look - M',
                        value: 'Baby Look - M',
                    },
                    {
                        text: 'Baby Look - G',
                        value: 'Baby Look - G',
                    },
                    {
                        text: 'Regular - P',
                        value: 'Regular - P',
                    },
                    {
                        text: 'Regular - M',
                        value: 'Regular - M',
                    },
                    {
                        text: 'Regular - G',
                        value: 'Regular - G',
                    },
                    {
                        text: 'Regular - GG',
                        value: 'Regular - GG',
                    },
                    {
                        text: 'Outro',
                        value: 'Outro',
                    },
                ]
            },
        ]
    }
};

/*
{
    name: `model`,
    label: `Modelo da Camiseta`,
    tip: false,
    required: true,
    validateErr: ``,
    cols: 12,
    type: 'select',
    options: [
        {
            text: 'Modelo 1',
            value: 1
        },
        {
            text: 'Modelo 2',
            value: 2
        },
        {
            text: 'Modelo 3',
            value: 3
        },
    ]
},
*/

export default products;