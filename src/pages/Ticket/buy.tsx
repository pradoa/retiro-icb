import React, { Component } from 'react';
import {
   Form, Row, Col, Input, Button, Tooltip, Checkbox, Select
} from 'antd';
import Header from 'components/PageStructure/Header';
import pagseguro from 'pagseguro.js';

import products from './products';

interface IProps {
   form: any;
}

interface IStates {
   formData: any;
   buySuccess: boolean;
   buyCode: string;
   checkoutUrl: string;
   productId: number;
}

export default class BuyTicket extends Component<IProps, IStates> {
   constructor(props: any) {
      super(props);

      let formData: any = {};
      formData.name = '';
      formData.email = '';
      formData.address = '';
      formData.ddd = 0;
      formData.phone = 0;
      formData.member = 0;
      formData.model = props.match.params.product_id === "1002" ? 1 : 0;
      formData.size = 'Outro';

      this.state = {
         formData,
         buySuccess: false,
         buyCode: '',
         checkoutUrl: '',
         productId: props.match.params.product_id,
      };

      this.handleBuy = this.handleBuy.bind(this);
   }

   getFields() {
      const { productId } = this.state;
      let { fields } = products[productId];

      return fields.map((f: any, i: number) => {
         const type = f.type ? f.type : 'text';

         switch (type) {
            default:
            case 'text': {
               return (
                  <Col span={f.cols ? f.cols : 24} key={i}>
                     <Form.Item
                        label={f.label}
                        hasFeedback
                        className="form-group"
                     >
                        <Tooltip
                           trigger={'focus'}
                           title={f.tip ? f.tip : null}
                           placement="topLeft"
                           overlayClassName="numeric-input"
                        >
                           <Input className="form-control" maxLength={f.max ? f.max : 999} placeholder={f.placeholder ? f.placeholder : f.label} id={f.name} onChange={(el: any) => { this.setFormValue(f.name, el.currentTarget.value); }} />
                        </Tooltip>
                     </Form.Item>
                  </Col>
               );
            }
            case 'checkbox': {
               return (
                  <Col span={f.cols ? f.cols : 24} key={i}>
                     <Form.Item
                        hasFeedback
                        className="form-group"
                     >
                        <Tooltip
                           trigger={'focus'}
                           title={f.tip ? f.tip : null}
                           placement="topLeft"
                           overlayClassName="numeric-input"
                        >
                           <Checkbox name={f.name} onChange={(el: any) => { this.setFormValue(f.name, el.target.checked ? 1 : 0); }} />
                           <div className="ant-form-item-label checkbox-label"><label>{f.label}</label></div>
                        </Tooltip>
                     </Form.Item>
                  </Col>
               );
            }
            case 'select': {
               return (
                  <Col span={f.cols ? f.cols : 24} key={i}>
                     <Form.Item
                        hasFeedback
                        className="form-group"
                     >
                        <Tooltip
                           trigger={'focus'}
                           title={f.tip ? f.tip : null}
                           placement="topLeft"
                           overlayClassName="numeric-input"
                        >
                           <Select
                              style={{ width: '100%' }}
                              placeholder={f.label}
                              optionFilterProp="children"
                              onChange={(el: any) => { console.log(el); this.setFormValue(f.name, el); }}
                           >
                              {
                                 f.options.map((o: any, ind: any) => {
                                    return (
                                       <Select.Option key={ind} value={o.value}>{o.text}</Select.Option>
                                    );
                                 })
                              }
                           </Select>
                        </Tooltip>
                     </Form.Item>
                  </Col>
               );
            }
         }
      });
   }

   handleBuy = (e: any) => {
      if (!this.onLoadState('send')) {

         this.setLoadState('send');
         e.preventDefault();

         const { productId } = this.state;
         let { name, value } = products[productId];

         let _this = this;
         const { formData } = _this.state;
         console.log('Received values of form: ', formData);

         if (formData.name.length < 5 || formData.email.length < 6 || formData.address.length < 10) {
            alert('Preencha os dados corretamente!');
            this.unsetLoadState('send');
            return;
         }

         // Ao iniciar a instância deve-se passar os dados do
         // vendedor para obter acesso à API.
         let buy = pagseguro({
            name: 'ICB',
            email: process.env.REACT_APP_PAGSEGURO_EMAIL,
            token: process.env.REACT_APP_PAGSEGURO_TOKEN,
         });

         buy.product.add({
            id: name,
            description: name,
            amount: value,
            quantity: 1,
            weight: 0
         });

         buy.sender.set({
            name: formData['name'],
            email: formData['email'],
            areaCode: formData['ddd'],
            phone: formData['phone'],
            address: formData['address']
         });

         buy.shipping.set({
            type: 3,
            cost: 0.00,
            postalCode: '09390-050',
            state: 'SP',
            city: 'Sao Paulo',
            district: 'Centro',
            street: 'Rua Luis Mariani',
            number: '56',
            complement: `${formData['member']}::${formData['model']}::${formData['size']}`
         });

         buy.checkout(function (err: any, res: any, body: any) {
            if (!!err === false && !!body.errors === false) {
               if (body.checkout.code) {
                  _this.setState({
                     buyCode: body.checkout.ref,
                     buySuccess: true,
                     checkoutUrl: `https://pagseguro.uol.com.br/v2/checkout/payment.html?code=${body.checkout.code}`
                  });
                  // grava os dados no banco de dados
               }
            } else {
               console.log(body);
               if (Array.isArray(body.errors.error))
                  body.errors.error = body.errors.error[0];
               if (body.errors.error.code) {
                  switch (body.errors.error.code) {
                     default: {
                        alert(`Error ${body.errors.error.code}`);
                        break;
                     }
                     case "11013": {
                        alert(`Digite um DDD válido e com apenas 2 dígitos!`);
                        break;
                     }
                     case "11014": {
                        alert(`Digite um telefone válido e com apenas números!`);
                        break;
                     }
                  }
               }
            }
            _this.unsetLoadState('send');
         });
      }
   }

   setFormValue(key: string, value: any) {
      const { formData } = this.state;
      formData[key] = value;
      this.setState({ formData });
   }

   renderForm() {

      const { productId } = this.state;
      let { name, value } = products[productId];

      return (
         <div className="row">
            <div className="col-md-6" data-aos="fade-up">
               <form action="#">
                  <Form
                     className="ant-advanced-search-form"
                  >
                     <Row gutter={24}>
                        {this.getFields()}
                     </Row>
                     <Row>
                        <Col span={24} style={{ textAlign: 'right' }}>
                           <Button type="primary" htmlType="submit" onClick={this.handleBuy}>
                              {
                                 this.onLoadState('send') ? (
                                    <span className="fa fas fa-spinner fa-spin" />
                                 ) : (
                                       "Concluir Inscrição"
                                    )
                              }
                           </Button>
                        </Col>
                     </Row>
                  </Form>
               </form>
            </div>
            <div className="col-md-5 ml-auto" data-aos="fade-up" data-aos-delay="100">
               <div className="p-4 mb-3">
                  <p className="mb-0 font-weight-bold text-secondary text-uppercase mb-3">TOTAL</p>

                  <p className="mb-4">{name}</p>

                  <h1 className="d-block mb-4" data-aos="fade-up" data-aos-delay="100">R$ {value.toFixed(2).replace('.', ',')}</h1>

               </div>
            </div>
         </div>
      );
   }

   renderSuccess() {
      const { buyCode, checkoutUrl } = this.state;

      return (
         <div className="row">
            <div className="col-md-6" data-aos="fade-up">
               <div className="p-4 mb-3">
                  <p className="mb-0 font-weight-bold text-secondary text-uppercase mb-3">Anote seu código de inscrição!</p>

                  <h1 className="d-block mb-4" data-aos="fade-up" data-aos-delay="100">{buyCode}</h1>

                  <Button type="primary" htmlType="submit" onClick={() => { window.location.href = checkoutUrl; }}>Pagar Online</Button>

                  <Button type="primary" ghost htmlType="submit" onClick={() => { window.open(`http://icbmaua.com.br/proxy/print.php?id=${buyCode}`, `blank`); }}>Pagar no Local</Button>

               </div>
            </div>
         </div>
      );
   }

   public render(): React.ReactNode {
      const { buySuccess } = this.state;
      return (
         <>
            <Header />
            <div className="site-section site-hero inner">
               <div className="container">
                  <div className="row align-items-center">
                     <div className="col-md-10">
                        <span className="d-block mb-3 caption" data-aos="fade-up">Conclua sua</span>
                        <h1 className="d-block mb-4" data-aos="fade-up" data-aos-delay="100">Inscrição</h1>
                     </div>
                  </div>
               </div>
            </div>

            <div className="site-section">
               <div className="container">
                  {
                     buySuccess ? this.renderSuccess() : this.renderForm()
                  }
               </div>
            </div>
         </>
      );
   }
}