import React, { Component } from 'react';
import Header from 'components/PageStructure/Header';

interface IProps {
}

interface IStates {
}

export default class Home extends Component<IProps, IStates> {
    public render(): React.ReactNode {
        return (
            <div>
                <Header />

                <div className="site-section site-hero inner">
                    <div className="container">
                        <div className="row align-items-center">
                            <div className="col-md-10">
                                <span className="d-block mb-3 caption">Conferência</span>
                                <h1 className="d-block mb-4">
                                    <img className="logo" width="100%" src={require('assets/images/logo1.png')} />
                                </h1>
                                <a href="/">
                                    <span className="d-block mb-3 caption">> 12 e 13 de Julho</span>
                                </a>
                                <br />
                                <br />
                                <br />
                                <div className="text-center"><a href="/ticket" className="btn btn-primary px-4 py-2">Inscreva-se</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
