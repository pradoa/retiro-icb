import React from 'react';
import { Route, BrowserRouter, Switch } from 'react-router-dom';

// global styles
import 'antd/dist/antd.css';
import 'assets/styles/style.css';

// pages
import Home from 'pages/Home';
import Ticket from 'pages/Ticket';
import BuyTicket from 'pages/Ticket/buy';

import NotFound from 'pages/NotFound';
import StatusList from './StatusList';

const Routes = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path='/' component={Home} />
                <Route exact path='/ticket' component={Ticket} />
                <Route exact path='/buy' component={BuyTicket} />
                <Route exact path='/buy/:product_id/' component={BuyTicket} />
                <Route exact path='/inscriptions' component={StatusList} />
                <Route path='*' component={NotFound} />
            </Switch>
        </BrowserRouter>
    );
};

export default Routes;
